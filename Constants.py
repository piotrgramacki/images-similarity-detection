import os

SOURCE_IMAGES_PATH = os.path.dirname(__file__) + "\\images\\"
CONVERTED_IMAGES_PATH = os.path.dirname(__file__) + "\\converted\\"
EXTRACTOR_PATH = os.path.dirname(__file__) + "\\features_extractor\\"
EXTRACTED_FEATURES_PATH = os.path.dirname(__file__) + "\\extracted_features\\"
BACKUP_PATH = os.path.dirname(__file__) + "\\backups\\"
LOG_PATH = os.path.dirname(__file__) + "\\logs\\"
