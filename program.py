from src.feature_extraction import *
from src.images_processing import *
from src.visualisation import *


def run(file1: str, file2: str):
    """
    Contains plenty of test cases used to create report
    :param file1: first image (.png format)
    :param file2: second image (.png format)
    """
    first_file = FeatureExtractor(filename=file1)
    second_file = FeatureExtractor(filename=file2)

    try:
        img1 = first_file.extract_image()
        img2 = second_file.extract_image()

        processor = ImagesProcessor(img1, img2)
        presenter = ImageVisualization(file1, file2, img1.coordinates, img2.coordinates)

        processor.calculate_pairs()
        print(len(processor.pairs))

        processor.calculate_coherent_pairs(75, 0.5)
        # print("10 -", len(processor.coherent))
        # res_10 = processor.coherent
        # processor.calculate_coherent_pairs(20, 0.5)
        # print("20 -", len(processor.coherent))
        # res_20 = processor.coherent
        # processor.calculate_coherent_pairs(50, 0.5)
        # print("50 -", len(processor.coherent))
        # res_50 = processor.coherent
        # processor.calculate_coherent_pairs(75, 0.5)
        # print("75 -", len(processor.coherent))
        # res_75 = processor.coherent
        # processor.calculate_coherent_pairs(100, 0.5)
        # print("100 -", len(processor.coherent))
        # res_100 = processor.coherent
        # processor.calculate_coherent_pairs(200, 0.5)
        # print("200 -", len(processor.coherent))
        # res_200 = processor.coherent
        # presenter.draw_from_list(res_10, "n = 10")
        # presenter.draw_from_list(res_20, "n = 20")
        # presenter.draw_from_list(res_50, "n = 50")
        # presenter.draw_from_list(res_75, "n = 75")
        # presenter.draw_from_list(res_100, "n = 100")
        # presenter.draw_from_list(res_200, "n = 200")

        # processor.calculate_coherent_pairs(50, 0.3)
        # print("0.3 -", len(processor.coherent))
        # res_30 = processor.coherent
        # processor.calculate_coherent_pairs(50, 0.4)
        # print("0.4 -", len(processor.coherent))
        # res_40 = processor.coherent
        # processor.calculate_coherent_pairs(50, 0.5)
        # print("0.5 -", len(processor.coherent))
        # res_50 = processor.coherent
        # processor.calculate_coherent_pairs(50, 0.6)
        # print("0.6 -", len(processor.coherent))
        # res_60 = processor.coherent
        # processor.calculate_coherent_pairs(50, 0.7)
        # print("0.7 -", len(processor.coherent))
        # res_70 = processor.coherent
        # presenter.draw_from_list(res_30, "cr = 0.3")
        # presenter.draw_from_list(res_40, "cr = 0.4")
        # presenter.draw_from_list(res_50, "cr = 0.5")
        # presenter.draw_from_list(res_60, "cr = 0.6")
        # presenter.draw_from_list(res_70, "cr = 0.7")

        # processor.calculate_transformation_model_perspective(100, 20)
        # res_100 = processor.perspective
        # print(len(processor.perspective))
        # processor.calculate_transformation_model_perspective(500, 20)
        # res_500 = processor.perspective
        # print(len(processor.perspective))
        # processor.calculate_transformation_model_perspective(1000, 20)
        # res_1000 = processor.perspective
        # print(len(processor.perspective))
        # processor.calculate_transformation_model_perspective(3000, 20)
        # res_3000 = processor.perspective
        # print(len(processor.perspective))
        # processor.calculate_transformation_model_perspective(5000, 20)
        # res_5000 = processor.perspective
        # print(len(processor.perspective))
        # processor.calculate_transformation_model_perspective(10000, 20)
        # res_10000 = processor.perspective
        # print(len(processor.perspective))
        # presenter.draw_from_list(res_100, "iter = 100")
        # presenter.draw_from_list(res_500, "iter = 500")
        # presenter.draw_from_list(res_1000, "iter = 1000")
        # presenter.draw_from_list(res_3000, "iter = 3000")
        # presenter.draw_from_list(res_5000, "iter = 5000")
        # presenter.draw_from_list(res_10000, "iter = 10000")

        # affine_start_time = time()
        processor.calculate_transformation_model_affine(1000, 5)
        # print(len(processor.affine))
        # affine_end_time = time()
        # perspective_start_time = time()
        processor.calculate_transformation_model_perspective(1000, 5)
        # print(len(processor.perspective))
        # perspective_end_time = time()
        #
        print(len(processor.pairs))
        print(len(processor.coherent))
        #
        # print("RANSAC affine time -", affine_end_time - affine_start_time)
        # print("RANSAC perspective time -", perspective_end_time - perspective_start_time)
        #
        presenter.draw_from_list(processor.pairs, "All pairs")
        presenter.draw_from_list(processor.coherent, "Coherent pairs")
        presenter.draw_from_list(processor.affine, "Affine pairs")
        presenter.draw_from_list(processor.perspective, "Perspective pairs")
        presenter.draw_from_model(processor.affine_model, "Affine transform")
        presenter.draw_from_model(processor.perspective_model, "Perspective transform")

        # for iters in [100, 500, 1000, 3000, 5000, 10000]:
        #     sum = 0
        #     time_sum = 0
        #     for i in range(10):
        #         start_time = time()
        #         processor.calculate_transformation_model_affine(iters, 50)
        #         end_time = time()
        #         sum = sum + len(processor.affine)
        #         time_sum = time_sum + end_time - start_time
        #     print(iters)
        #     print(sum / 10)
        #     print(time_sum / 10)
        #     print()

        # processor.calculate_transformation_model_affine(1000, 1, log_file="1 - affine 1000")
        # res_1 = processor.affine_model
        # processor.calculate_transformation_model_affine(1000, 5)
        # res_5 = processor.affine_model
        # processor.calculate_transformation_model_affine(1000, 10)
        # res_10 = processor.affine_model
        # processor.calculate_transformation_model_affine(1000, 20)
        # res_20 = processor.affine_model
        # processor.calculate_transformation_model_affine(1000, 35)
        # res_35 = processor.affine_model
        # processor.calculate_transformation_model_affine(1000, 50)
        # res_50 = processor.affine_model
        # presenter.draw_from_model(res_1, "me = 1")
        # presenter.draw_from_model(res_5, "me = 5")
        # presenter.draw_from_model(res_10, "me = 10")
        # presenter.draw_from_model(res_20, "me = 20")
        # presenter.draw_from_model(res_35, "me = 35")
        # presenter.draw_from_model(res_50, "me = 50")

        # for i in range(5):
        #     processor.calculate_transformation_model_perspective(1000, 10, distance_heuristic=True,
        #                                                          log_file="distance-{}".format(i))

        # processor.calculate_transformation_model_affine(0, 20, iteration_heuristic=0.7)
        # processor.calculate_transformation_model_affine(0, 20, iteration_heuristic=0.8)
        # processor.calculate_transformation_model_affine(0, 20, iteration_heuristic=0.9)
        # processor.calculate_transformation_model_affine(0, 20, iteration_heuristic=0.99)
        #
        # processor.calculate_transformation_model_perspective(0, 20, iteration_heuristic=0.7)
        # processor.calculate_transformation_model_perspective(0, 20, iteration_heuristic=0.8)
        # processor.calculate_transformation_model_perspective(0, 20, iteration_heuristic=0.9)
        # processor.calculate_transformation_model_perspective(0, 20, iteration_heuristic=0.99)

        # affine_no = 0
        # affine_with = 0
        # persp_no = 0
        # persp_with = 0
        # for i in range(5):
        #     processor.calculate_transformation_model_affine(918, 20)
        #     affine_with = affine_with + len(processor.affine)
        #
        #     processor.calculate_transformation_model_affine(2000, 20)
        #     affine_no = affine_no + len(processor.affine)
        #
        #     processor.calculate_transformation_model_perspective(6766, 20)
        #     persp_with = persp_with + len(processor.perspective)
        #
        #     processor.calculate_transformation_model_perspective(15000, 20)
        #     persp_no = persp_no + len(processor.perspective)

        # print("A no -", affine_no / 5)
        # print("A with -", affine_with / 5)
        # print("P no -", persp_no / 5)
        # print("P with -", persp_with / 5)

    except WrongFilenameException:
        print("Wrong filenames")


if __name__ == '__main__':
    # if len(sys.argv) < 3:
    #     print("Missing filenames")
    # else:
    #     run(sys.argv[1], sys.argv[2])
    run("book1.png", "book2.png")  # nice result
    # run("shoe1.png", "shoe2.png")  # quite nice result
    # run("hala1.png", "hala2.png")
    # run("ebook2.png", "ebook1.png")  # por results
    # run("book1.png", "book2.png")
