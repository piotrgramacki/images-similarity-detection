import subprocess
from typing import Dict, Tuple

import numpy as np
from PIL import Image

from Constants import *


class WrongFilenameException(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class ExtractedImage:
    def __init__(self, coordinates: Dict[int, Tuple[int, int]], coords_array: np.ndarray, features: np.ndarray,
                 name: str):
        self.coordinates = coordinates
        self.coordinates_array = coords_array
        self.features = features
        self.filename = name


class FeatureExtractor:
    def __init__(self, filename):
        self.filename: str = filename
        self.coordinates: Dict[int, Tuple[int, int]] = {}
        self.features = None
        self.coords_array = None

    def _check_file(self):
        images = os.listdir(SOURCE_IMAGES_PATH)
        return self.filename in images

    def _convert_image(self):
        image = Image.open(SOURCE_IMAGES_PATH + self.filename)
        converted_image = image.convert('RGB').convert('P', palette=Image.ADAPTIVE)
        converted_image.save(CONVERTED_IMAGES_PATH + self.filename)

    def _extract_features(self):
        subprocess.run([EXTRACTOR_PATH + "extract_features_32bit.exe", "-haraff", "-sift", "-i",
                        CONVERTED_IMAGES_PATH + self.filename, "-DE"])
        os.system("move " + CONVERTED_IMAGES_PATH + self.filename + ".h* " + EXTRACTED_FEATURES_PATH)

    def _read_features(self):
        file = open(EXTRACTED_FEATURES_PATH + self.filename + ".haraff.sift")
        lines = file.readlines()

        number_of_features: int = int(lines[0])
        number_of_key_points: int = int(lines[1])

        self.features = np.empty((number_of_key_points, number_of_features), dtype=int)
        self.coords_array = np.empty((number_of_key_points, 2), dtype=int)

        for i in range(number_of_key_points):
            line_split = lines[i + 2].split(" ")
            self.coordinates[i] = (int(float(line_split[0])), int(float(line_split[1])))
            self.coords_array[i, :] = [int(float(line_split[0])), int(float(line_split[1]))]
            self.features[i, :] = line_split[5:]

    def extract_image(self) -> ExtractedImage:
        if self._check_file():
            self._convert_image()
            self._extract_features()
            self._read_features()
            print("Extracted image", self.filename)
            return ExtractedImage(self.coordinates, self.coords_array, self.features, self.filename)
        else:
            raise WrongFilenameException
