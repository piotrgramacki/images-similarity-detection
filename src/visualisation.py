import random
from typing import *

import cv2
import numpy as np

from Constants import *
from src.images_processing import KeyPointPair


class ImageVisualization:
    def __init__(self,
                 img1_name: str,
                 img2_name: str,
                 coords1: Dict[int, Tuple[int, int]],
                 coords2: Dict[int, Tuple[int, int]]):
        self.image1 = img1_name
        self.image2 = img2_name
        self.coordinates1 = coords1
        self.coordinates2 = coords2

    def draw_from_list(self, pairs: List[KeyPointPair], label: str):
        img1 = cv2.imread(CONVERTED_IMAGES_PATH + self.image1)
        img2 = cv2.imread(CONVERTED_IMAGES_PATH + self.image2)
        joined_image = np.vstack((img1, img2))

        offset_vertical = img1.shape[0]

        for pair in pairs:
            point1 = self.coordinates1[pair.first]
            point2 = self.coordinates2[pair.second]

            cv2.line(img=joined_image, pt1=(point1[0], point1[1]),
                     pt2=(point2[0], point2[1] + offset_vertical),
                     color=(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)),
                     thickness=1, lineType=cv2.LINE_AA)

        cv2.namedWindow(label, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(label, joined_image.shape[1] // 2, joined_image.shape[0] // 2)
        cv2.imshow(label, joined_image)

        cv2.waitKey(0)

    def draw_from_model(self, model: np.ndarray(shape=(3, 3)), label: str):
        img1 = cv2.imread(CONVERTED_IMAGES_PATH + self.image1)
        img2 = cv2.imread(CONVERTED_IMAGES_PATH + self.image2)
        if model[2][0] == 0 and model[2][1] == 0:
            img1 = cv2.warpAffine(img1, model[:2], (img1.shape[1], img1.shape[0]))
        else:
            img1 = cv2.warpPerspective(img1, model, (img1.shape[1], img1.shape[0]))
        joined_image = np.vstack((img1, img2))

        cv2.namedWindow(label, cv2.WINDOW_NORMAL)
        cv2.resizeWindow(label, joined_image.shape[1] // 2, joined_image.shape[0] // 2)
        cv2.imshow(label, joined_image)

        cv2.waitKey(0)

    @staticmethod
    def _transform_with_model(point: Tuple[int, int], model):
        start = np.array([
            [point[0]],
            [point[1]],
            [1]
        ])

        end = model @ start

        return int(end[0][0]), int(end[1][0])
