import pickle as pkl
import random
from typing import List, Dict

import math
import numpy as np

from Constants import *
from src.feature_extraction import ExtractedImage


class KeyPointPair:
    def __init__(self, first_id: int, second_id: int):
        self.first: int = first_id
        self.second: int = second_id


def store_best_model(scores, filename):
    with open(LOG_PATH + filename + ".csv", "w+") as file:
        for iteration, score in enumerate(scores):
            file.write("{}; {}\n".format(iteration, score))


class ImagesProcessor:
    def __init__(self, image1: ExtractedImage, image2: ExtractedImage):
        self.pairs: List[KeyPointPair] = []
        self.coherent: List[KeyPointPair] = []
        self.affine: List[KeyPointPair] = []
        self.perspective: List[KeyPointPair] = []
        self.pairs_dict: Dict[int, int] = {}
        self.image1: ExtractedImage = image1
        self.image2: ExtractedImage = image2
        self.affine_model: np.ndarray = np.zeros((3, 3))
        self.perspective_model: np.ndarray = np.zeros((3, 3))
        self.pairs_for_model = list(self.pairs)
        self.filename = self.image1.filename + self.image2.filename + "backup.pkl"

        self.evaluation_data = None

    def _backup_loaded(self):
        files = os.listdir(BACKUP_PATH)
        if self.filename in files:
            with open(BACKUP_PATH + self.filename, 'rb') as file:
                self.pairs = pkl.load(file)

            for pair in self.pairs:
                self.pairs_dict[pair.first] = pair.second

            return True
        return False

    def _store_backup(self):
        with open(BACKUP_PATH + self.filename, 'wb') as file:
            pkl.dump(self.pairs, file)

    def calculate_pairs(self):
        print("Pairs - start")
        if not self._backup_loaded():
            first_image = self.image1.features
            n = first_image.shape[0]
            second_image = self.image2.features
            m = second_image.shape[0]

            neighbours_of_first = np.empty(n, dtype=int)
            neighbours_of_second = np.empty(m, dtype=int)

            for i in range(max(n, m)):
                if i < n:
                    neighbours_of_first[i] = np.argmin(np.sum(np.abs(first_image[i, :] - second_image), 1))
                if i < m:
                    neighbours_of_second[i] = np.argmin(np.sum(np.abs(first_image - second_image[i, :]), 1))

            for i in range(n):
                if neighbours_of_second[neighbours_of_first[i]] == i:
                    self.pairs.append(KeyPointPair(i, int(neighbours_of_first[i])))
                    self.pairs_dict[i] = int(neighbours_of_first[i])

            self._store_backup()

        print("Pairs - end")

    def calculate_coherent_pairs(self, size: int, approval: float):
        print("Coherent - start")

        self.coherent = []

        points_in_pairs1 = list(map(lambda p: p.first, self.pairs))
        points_in_pairs2 = list(map(lambda p: p.second, self.pairs))

        for pair in self.pairs:
            neighbourhood1 = ImagesProcessor._get_neighbourhood(pair.first, points_in_pairs1, self.image1, size)
            neighbourhood2 = ImagesProcessor._get_neighbourhood(pair.second, points_in_pairs2, self.image2, size)

            counter = 0

            for point in neighbourhood1:
                if self.pairs_dict[point] in neighbourhood2:
                    counter = counter + 1

            coherence_ratio = counter / len(neighbourhood1)
            if coherence_ratio >= approval:
                self.coherent.append(pair)

        print("Coherent - end")

    @staticmethod
    def _get_neighbourhood(point: int, all_points: List[int], image: ExtractedImage, size: int) -> List[int]:
        # TODO - can't remove "point" element because all_points can't change,
        #  but it should make no difference
        # all_points.remove(point)
        others = image.coordinates_array[all_points]
        current = image.coordinates_array[point, :]

        distances = np.sqrt(np.sum((current - others) ** 2, 1))

        distances_with_indices = list(zip(all_points, distances))

        distances_with_indices.sort(key=lambda pair_id_distance: pair_id_distance[1])

        sorted_indices = list(tuple(zip(*distances_with_indices))[0])
        return sorted_indices[1:size]

    def calculate_transformation_model_affine(self, iterations: int, max_distance: int,
                                              distribution_heuristic: bool = False,
                                              iteration_heuristic: float = 0.,
                                              distance_heuristic: bool = False,
                                              log_file=None):
        best_score = 0
        scores_log = []
        self.pairs_for_model = list(self.pairs)
        if iteration_heuristic != 0.:
            iterations = self._get_iterations_number(iteration_heuristic, 3)

        print("Affine iterations -", iterations)
        for i in range(iterations):
            # print("AFFINE RANSAC -", i)

            model, pairs = self._random_model_affine(distance_heuristic)

            score = self._evaluate_model(model, max_distance)

            if score > best_score:
                self.affine_model = model
                best_score = score
                if distribution_heuristic:
                    self.pairs_for_model.extend(pairs)
            scores_log.append(best_score)
        self._safe_affine_pairs(max_distance)

        if log_file is not None:
            store_best_model(scores_log, log_file)

    def calculate_transformation_model_perspective(self, iterations: int, max_distance: int,
                                                   distribution_heuristic: bool = False,
                                                   iteration_heuristic: float = 0.,
                                                   distance_heuristic: bool = False,
                                                   log_file=None):
        best_score = 0
        scores_log = []
        self.pairs_for_model = list(self.pairs)
        if iteration_heuristic != 0.:
            iterations = self._get_iterations_number(iteration_heuristic, 4)

        print("Perspective iterations -", iterations)
        for i in range(iterations):
            # print("PERSPECTIVE RANSAC -", i)

            model, pairs = self._random_model_perspective(distance_heuristic)

            score = self._evaluate_model(model, max_distance)

            if score > best_score:
                self.perspective_model = model
                best_score = score
                if distribution_heuristic:
                    self.pairs_for_model.extend(pairs)
            scores_log.append(best_score)
        self._safe_perspective_pairs(max_distance)

        if log_file is not None:
            store_best_model(scores_log, log_file)

    def _safe_affine_pairs(self, max_error: int):
        if self.evaluation_data is None:
            self._create_evaluation_data()
            print("Evaluation data created")

        points_first = self.evaluation_data[0]
        points_second = self.evaluation_data[1]

        points_from_model = self.affine_model @ points_first
        points_from_model = points_from_model / points_from_model[2]
        distances = np.sqrt(np.sum((points_second - points_from_model) ** 2, 0))
        within_max_error = distances < max_error

        self.affine = [pair for index, pair in enumerate(self.pairs) if within_max_error[index]]

    def _safe_perspective_pairs(self, max_error: int):
        if self.evaluation_data is None:
            self._create_evaluation_data()
            print("Evaluation data created")

        points_first = self.evaluation_data[0]
        points_second = self.evaluation_data[1]

        points_from_model = self.perspective_model @ points_first
        points_from_model = points_from_model / points_from_model[2]
        distances = np.sqrt(np.sum((points_second - points_from_model) ** 2, 0))
        within_max_error = distances < max_error

        self.perspective = [pair for index, pair in enumerate(self.pairs) if within_max_error[index]]

    def _random_model_affine(self, use_heuristic: bool = False):
        if not use_heuristic:
            selected_pairs = random.sample(self.pairs_for_model, 3)
        else:
            selected_pairs = self._pairs_with_distance_heuristic(3)

        x1 = self.image1.coordinates[selected_pairs[0].first][0]
        x2 = self.image1.coordinates[selected_pairs[1].first][0]
        x3 = self.image1.coordinates[selected_pairs[2].first][0]
        y1 = self.image1.coordinates[selected_pairs[0].first][1]
        y2 = self.image1.coordinates[selected_pairs[1].first][1]
        y3 = self.image1.coordinates[selected_pairs[2].first][1]

        u1 = self.image2.coordinates[selected_pairs[0].second][0]
        u2 = self.image2.coordinates[selected_pairs[1].second][0]
        u3 = self.image2.coordinates[selected_pairs[2].second][0]
        v1 = self.image2.coordinates[selected_pairs[0].second][1]
        v2 = self.image2.coordinates[selected_pairs[1].second][1]
        v3 = self.image2.coordinates[selected_pairs[2].second][1]

        affine_matrix = np.array([
            [x1, y1, 1, 0, 0, 0],
            [x2, y2, 1, 0, 0, 0],
            [x3, y3, 1, 0, 0, 0],
            [0, 0, 0, x1, y1, 1],
            [0, 0, 0, x2, y2, 1],
            [0, 0, 0, x3, y3, 1]
        ])

        second_image_coords = np.array([
            [u1],
            [u2],
            [u3],
            [v1],
            [v2],
            [v3],
        ])

        try:
            values = np.linalg.inv(affine_matrix) @ second_image_coords
        except np.linalg.LinAlgError:
            values = np.ones(shape=(6, 1))

        return np.array([
            [values[0][0], values[1][0], values[2][0]],
            [values[3][0], values[4][0], values[5][0]],
            [0, 0, 1]
        ]), selected_pairs

    def _random_model_perspective(self, use_heuristic: bool = False):
        if not use_heuristic:
            selected_pairs = random.sample(self.pairs_for_model, 4)
        else:
            selected_pairs = self._pairs_with_distance_heuristic(4)

        x1 = self.image1.coordinates[selected_pairs[0].first][0]
        x2 = self.image1.coordinates[selected_pairs[1].first][0]
        x3 = self.image1.coordinates[selected_pairs[2].first][0]
        x4 = self.image1.coordinates[selected_pairs[3].first][0]
        y1 = self.image1.coordinates[selected_pairs[0].first][1]
        y2 = self.image1.coordinates[selected_pairs[1].first][1]
        y3 = self.image1.coordinates[selected_pairs[2].first][1]
        y4 = self.image1.coordinates[selected_pairs[3].first][1]

        u1 = self.image2.coordinates[selected_pairs[0].second][0]
        u2 = self.image2.coordinates[selected_pairs[1].second][0]
        u3 = self.image2.coordinates[selected_pairs[2].second][0]
        u4 = self.image2.coordinates[selected_pairs[3].second][0]
        v1 = self.image2.coordinates[selected_pairs[0].second][1]
        v2 = self.image2.coordinates[selected_pairs[1].second][1]
        v3 = self.image2.coordinates[selected_pairs[2].second][1]
        v4 = self.image2.coordinates[selected_pairs[3].second][1]

        perspective_matrix = np.array([
            [x1, y1, 1, 0, 0, 0, (-1) * u1 * x1, (-1) * u1 * y1],
            [x2, y2, 1, 0, 0, 0, (-1) * u2 * x2, (-1) * u2 * y2],
            [x3, y3, 1, 0, 0, 0, (-1) * u3 * x3, (-1) * u3 * y3],
            [x4, y4, 1, 0, 0, 0, (-1) * u4 * x4, (-1) * u4 * y4],
            [0, 0, 0, x1, y1, 1, (-1) * v1 * x1, (-1) * v1 * y1],
            [0, 0, 0, x2, y2, 1, (-1) * v2 * x2, (-1) * v2 * y2],
            [0, 0, 0, x3, y3, 1, (-1) * v3 * x3, (-1) * v3 * y3],
            [0, 0, 0, x4, y4, 1, (-1) * v4 * x4, (-1) * v4 * y4]
        ])

        second_image_coords = np.array([
            [u1],
            [u2],
            [u3],
            [u4],
            [v1],
            [v2],
            [v3],
            [v4]
        ])

        try:
            values = np.linalg.inv(perspective_matrix) @ second_image_coords
        except np.linalg.LinAlgError:
            values = np.ones(shape=(8, 1))

        return np.array([
            [values[0][0], values[1][0], values[2][0]],
            [values[3][0], values[4][0], values[5][0]],
            [values[6][0], values[7][0], 1]
        ]), selected_pairs

    def _evaluate_model(self, model: np.ndarray(shape=(3, 3)), max_error: int) -> int:
        if self.evaluation_data is None:
            self._create_evaluation_data()
            print("Evaluation data created")

        points_first = self.evaluation_data[0]
        points_second = self.evaluation_data[1]

        points_from_model = model @ points_first
        points_from_model = points_from_model / points_from_model[2]
        distances = np.sqrt(np.sum((points_second - points_from_model) ** 2, 0))
        within_max_error = distances < max_error
        score = np.sum(within_max_error)

        # score = np.sum(np.sqrt(np.sum((points_second - points_from_model) ** 2, 0)) < max_error)

        return int(score)

    def _create_evaluation_data(self):
        m = len(self.pairs)
        first = np.ones((3, m), dtype=int)
        second = np.ones((3, m), dtype=int)
        for i in range(m):
            first[0, i] = self.image1.coordinates[self.pairs[i].first][0]
            first[1, i] = self.image1.coordinates[self.pairs[i].first][1]
            second[0, i] = self.image2.coordinates[self.pairs[i].second][0]
            second[1, i] = self.image2.coordinates[self.pairs[i].second][1]
        self.evaluation_data = first, second

    def _get_iterations_number(self, p, n):
        w = len(self.coherent) / len(self.pairs)
        return int(math.log2(1 - p) / math.log2(1 - w ** n))

    def _pairs_with_distance_heuristic(self, count) -> List[KeyPointPair]:
        r = 4
        R = 120

        while True:
            pairs_to_choose: List[KeyPointPair] = list(self.pairs)
            selected_pairs: List[KeyPointPair] = random.sample(pairs_to_choose, 1)
            for i in range(count):
                selected = selected_pairs[i]
                filter(lambda p: self._check_distance(selected, p, r, R), pairs_to_choose)
                if len(pairs_to_choose) > 0:
                    new_pair = random.sample(pairs_to_choose, 1)[0]
                    selected_pairs.append(new_pair)
                    if len(selected_pairs) == count:
                        return selected_pairs
                else:
                    break

    def _check_distance(self, pair1: KeyPointPair, pair2: KeyPointPair, r, R):
        distance1 = math.sqrt(
            (self.image1.coordinates[pair1.first][0] - self.image1.coordinates[pair2.first][0]) ** 2
            +
            (self.image1.coordinates[pair1.first][1] - self.image1.coordinates[pair2.first][1]) ** 2
        )
        distance2 = math.sqrt(
            (self.image2.coordinates[pair1.second][0] - self.image2.coordinates[pair2.second][0]) ** 2
            +
            (self.image2.coordinates[pair1.second][1] - self.image2.coordinates[pair2.second][1]) ** 2
        )
        return (r < distance1 < R) and (r < distance2 < R)
